'use strict';

// Declare app level module which depends on views, and components
var vtapp = angular.module('VTapp', ['ngRoute', 'selectionModel']);

vtapp.config(function($routeProvider){
  $routeProvider.when("/",
    {
      templateUrl: "view1/view1.html",
      controller: "AppCtrl"
    })
  	.when("/view1",
    {
      templateUrl: "view1/view1.html",
      controller: "AppCtrl"
    }) 
  	.when("/view2",
    {
      templateUrl: "view2/view2.html",
      controller: "selectedController"
    });
});



vtapp.controller('AppCtrl', function($scope, arrayShare, items){

	var app = this;

	app.body = $scope;

	this.role= "";
	//var votes= [];


	//these are all of the variables that sit within each of the votable panels
	$scope.votables = [
		{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 0,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 1,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 2,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 3,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 4,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 5,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 6,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 7,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 8,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 9,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 10,
			binary: 0
		},{
			img: 'http://www.placecage.com/g/200/200',
			role: 'A votable',
			index: 11,
			binary: 0
		}
	];

	
	this.changed = function(votables) {

	}

	//add to array from directive
	$scope.VTadd = function () {

	}

	$scope.votes  = [];	//initialise the array
	$scope.selectedItems = [];

	//$scope.selectedItems = items.list;

	$scope.arrayCreate = function(votable, $event) {

		console.log('Selected Items ' + this.selectedItems.length);


		var role = $event.currentTarget,
			roleForm = $(role).attr('data-index'); //get attribute of selected

		if($(role).hasClass('selected') !== true) {

			var inx = $scope.votes.indexOf(votable.index);	//get index of unselected
			$scope.votes.splice(inx, 1); //remove that one

			votable.binary = 0; //set binary to 0

			arrayShare.set($scope.votes);
			console.log($scope.votes);

			/***cache thing***/
			// HackerNewsService.setNews('myArticle', $scope.votes);
			// $scope.article = HackerNewsService.getNews('myArticle');

			// console.log($scope.article);
			/***cache thing***/

		} else {

			$scope.votes.push(votable.index); //add index to $scope.votes array

			votable.binary = 1; //set binary to 1

			arrayShare.set($scope.votes);
			console.log($scope.votes);


	
		}	

		return $scope.votes;

	}

});


vtapp.factory('items', function() {
    var items = [];
    //var itemsService = {};
    
    items.list = function() {
        return items;
    };
    
    return items;
});



vtapp.factory('arrayShare', function() {

	 var savedData = {}
	 function set(data) {
	   savedData = data;
	 }
	 function get() {
	  return savedData;
	 }

	 return {
	  set: set,
	  get: get
	 }

});


vtapp.controller('panelController', function($scope, arrayShare){

	var app = this;

	app.message = "SELECT";
	app.class = "inactive";

	$scope.getIndex = arrayShare.get();

});


vtapp.controller('selectedController', function($scope, arrayShare){

	var app = this;

	$scope.getIndex = arrayShare.get();

});


//turn on selected elements
// vtapp.filter('turnOnSelected', function(arrayShare) {

// 	var getIndex = arrayShare.get();

// 	//console.log('>>>>>>> ' + getIndex);

// 	function getByIndex(arr, index) {
// 	    for (var d = 0, len = arr.length; d < len; d += 1) {
// 	        if (arr[d].index === index) {
// 	        	console.log(arr[d].binary);
// 	        	arr[d].binary = 1;
// 	            return arr[d];
// 	        } else {
// 	        	arr[d].binary = 0;
// 	            return arr[d];
// 	        }
// 	    }
// 	}

//     return function(items) {


//     	if(getIndex > -1) {

//     		console.log('greater than');

// 	    	var filtered = []; 

// 	    	for (var j=0; j < getIndex.length; j++) {

// 	        	var item = getByIndex(items, getIndex[j]);
// 	        	filtered.push(item);

// 	    	}

// 	        console.log(filtered);
// 	        return filtered;

// 		} else {

// 			console.log('negative 1');

// 		    return items;

// 		}

//     };


// });




//Filter output on second results page
vtapp.filter('selectedVotables', function(arrayShare) {

	var getIndex = arrayShare.get();

	//console.log('>>>>>>> ' + getIndex);

    function getByIndex(arr, index) {
	    for (var d = 0, len = arr.length; d < len; d += 1) {
	        if (arr[d].index === index) {
	            return arr[d];
	        }
	    }
	}

    return function(items) {

    	var filtered = []; 

    	for (var j=0; j < getIndex.length; j++) {

        	var item = getByIndex(items, getIndex[j]);
        	filtered.push(item);

    	}

        return filtered;

    };
});


//add votable elements
vtapp.directive("vtadd", function(){
	return {

		restrict: "A",
		link: function(scope, element, attrs) {
			element.on("click", function(){


				//click behaviour

				//word on item

				if (element.hasClass('dontClick')) {
			        return false;
			    }

				if(element.hasClass(attrs.activate) !== true) {

					scope.$apply(attrs.vtadd); //add the value to the array

					element.text(attrs.activate).addClass(attrs.activate).parent().css('background-color', '#D4FEBA');
					element.text(attrs.activate).next().addClass(attrs.activate);

				} else {

					scope.$apply(attrs.vtremove); //add the value to the array

					element.removeClass(attrs.activate).text(scope.panel.message).parent().css('background-color', '#efefef');
					element.next().removeClass(attrs.activate);

				}


			});
		
		}
	}
});

